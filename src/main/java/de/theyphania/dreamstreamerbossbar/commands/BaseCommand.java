package de.theyphania.dreamstreamerbossbar.commands;

import de.theyphania.dreamstreamerbossbar.BossBarHandler;
import de.theyphania.dreamstreamerbossbar.DreamStreamerBossBar;
import dev.jorel.commandapi.annotations.Alias;
import dev.jorel.commandapi.annotations.Command;
import dev.jorel.commandapi.annotations.Default;
import dev.jorel.commandapi.annotations.Permission;
import dev.jorel.commandapi.annotations.Subcommand;
import dev.jorel.commandapi.annotations.arguments.AGreedyStringArgument;
import dev.jorel.commandapi.annotations.arguments.AMultiLiteralArgument;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.command.CommandSender;

@Command("dsbb")
@Alias({"dreamstreamerbossbar", "dreamstreamerbossbar:dsbb", "dreamstreamerbossbar:dreamstreamerbossbar"})
@Permission("dreamstreamer.command.basecommand")
public class BaseCommand
{
	private static final DreamStreamerBossBar plugin = DreamStreamerBossBar.getInstance();
	private static final BossBarHandler bossBarHandler = BossBarHandler.getInstance();
	private static final Component prefixComponent = Component.text("[").color(TextColor.color(0x0D0D0D))
														 .append(Component.text(plugin.getPrefix()).color(TextColor.color(0x2B3966)).hoverEvent(HoverEvent.showText(Component.text(plugin.getName()).color(TextColor.color(0x2B3966)))))
														 .append(Component.text("] ").color(TextColor.color(0x0D0D0D)));
	
	@Default
	public static void dsbb(CommandSender sender)
	{
		Component finalComponent = prefixComponent.append(Component.text("Folgende Befehle kannst du nutzen:\n/dsbb titel <Titel>\n/dsbb farbe <Farbe>\n/dsbb stil <Stil>\n/dsbb reload").color(TextColor.color(0x888888)));
		sender.sendMessage(finalComponent);
	}
	
	@Subcommand("titel")
	public static void dsbbTitel(CommandSender sender, @AGreedyStringArgument String titel)
	{
		bossBarHandler.setTitle(titel);
		plugin.getConfig().set("allgemein.titel", titel);
		plugin.savePluginConfiguration();
		Component finalComponent = prefixComponent.append(Component.text("Titel erfolgreich auf ").color(TextColor.color(0x888888)).append(plugin.translateColorCodes(titel)).append(Component.text(" geändert.").color(TextColor.color(0x888888))));
		sender.sendMessage(finalComponent);
	}
	
	@Subcommand("farbe")
	public static void dsbbColor(CommandSender sender, @AMultiLiteralArgument({"blue", "green", "pink", "purple", "red", "white", "yellow"}) String farbe)
	{
		bossBarHandler.setColor(farbe.toUpperCase());
		plugin.getConfig().set("allgemein.farbe", farbe);
		plugin.savePluginConfiguration();
		Component finalComponent = prefixComponent.append(Component.text("Farbe erfolgreich aktualisiert.").color(TextColor.color(0x888888)));
		sender.sendMessage(finalComponent);
	}
	
	@Subcommand("stil")
	public static void dsbbStil(CommandSender sender, @AMultiLiteralArgument({"progress", "notched_6", "notched_10", "notched_12", "notched_20"}) String stil)
	{
		bossBarHandler.setOverlay(stil.toUpperCase());
		plugin.getConfig().set("allgemein.stil", stil);
		plugin.savePluginConfiguration();
		Component finalComponent = prefixComponent.append(Component.text("Stil erfolgreich aktualisiert.").color(TextColor.color(0x888888)));
		sender.sendMessage(finalComponent);
	}
	
	@Subcommand("reload")
	public static void dsbbReload(CommandSender sender)
	{
		plugin.reloadPluginConfiguration();
		bossBarHandler.setTitle(plugin.getConfig().getString("allgemein.titel"));
		bossBarHandler.setColor(plugin.getConfig().getString("allgemein.farbe").toUpperCase());
		bossBarHandler.setOverlay(plugin.getConfig().getString("allgemein.stil").toUpperCase());
		Component finalComponent = prefixComponent.append(Component.text("Konfiguration erfolgreich neugeladen.").color(TextColor.color(0x888888)));
		sender.sendMessage(finalComponent);
	}
}
