package de.theyphania.dreamstreamerbossbar;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.bossbar.BossBar;
import net.kyori.adventure.bossbar.BossBar.Color;
import net.kyori.adventure.bossbar.BossBar.Flag;
import net.kyori.adventure.bossbar.BossBar.Overlay;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;

public class BossBarHandler
{
	private final DreamStreamerBossBar plugin = DreamStreamerBossBar.getInstance();
	private final BossBar bossBar;
	private final Color defaultColor = Color.PURPLE;
	private final Overlay defaultOverlay = Overlay.PROGRESS;
	
	private static BossBarHandler instance;
	
	public BossBarHandler()
	{
		instance = this;
		Component title = Component.text("Initialisiere...");
		bossBar = BossBar.bossBar(title, 1F, defaultColor, defaultOverlay);
		
		setTitle(plugin.getConfig().getString("allgemein.titel"));
		setColor(plugin.getConfig().getString("allgemein.farbe"));
		setOverlay(plugin.getConfig().getString("allgemein.stil"));
	}
	
	public static BossBarHandler getInstance()
	{
		return instance;
	}
	
	public void setTitle(String titleString)
	{
		Component name = plugin.translateColorCodes(titleString);
		bossBar.name(name);
	}
	
	public void setColor(String colorString)
	{
		Color tempColor = Color.valueOf(colorString.toUpperCase());
		if (tempColor == null) tempColor = defaultColor;
		bossBar.color(tempColor);
	}
	
	public void setOverlay(String overlayString)
	{
		Overlay tempOverlay = Overlay.valueOf(overlayString.toUpperCase());
		if (tempOverlay == null) tempOverlay = defaultOverlay;
		bossBar.overlay(tempOverlay);
	}
	
	public void setFlag(String flagString, boolean enabled)
	{
		Flag tempFlag = Flag.valueOf(flagString.toUpperCase());
		if (tempFlag == null) return;
		if (bossBar.hasFlag(tempFlag) && !enabled)
		{
			bossBar.removeFlag(tempFlag);
		}
		if (!bossBar.hasFlag(tempFlag) && enabled)
		{
			bossBar.addFlag(tempFlag);
		}
	}
	
	public void show()
	{
		Audience audience = Audience.audience(Bukkit.getOnlinePlayers());
		audience.showBossBar(bossBar);
	}
}
