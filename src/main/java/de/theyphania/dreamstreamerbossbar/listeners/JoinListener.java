package de.theyphania.dreamstreamerbossbar.listeners;

import de.theyphania.dreamstreamerbossbar.BossBarHandler;
import de.theyphania.dreamstreamerbossbar.DreamStreamerBossBar;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener
{
	private final DreamStreamerBossBar plugin = DreamStreamerBossBar.getInstance();
	
	private static JoinListener instance;
	
	public JoinListener()
	{
		instance = this;
		Bukkit.getPluginManager().registerEvents(this, plugin);
		plugin.debug("... PlayerJoinEvent");
	}
	
	public static JoinListener getInstance()
	{
		return instance;
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void PlayerJoinEvent(PlayerJoinEvent event)
	{
		BossBarHandler.getInstance().show();
	}
}
