package de.theyphania.dreamstreamerbossbar;

import de.theyphania.dreamstreamerbossbar.commands.BaseCommand;
import de.theyphania.dreamstreamerbossbar.listeners.JoinListener;
import dev.jorel.commandapi.CommandAPI;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.logging.Level;

public class DreamStreamerBossBar extends DreamStreamerBossBarPlugin
{
	private YamlConfiguration pluginConfiguration;
	private File configurationFile;
	
	private static DreamStreamerBossBar instance;
	
	public DreamStreamerBossBar()
	{
		instance = this;
	}
	
	public static DreamStreamerBossBar getInstance()
	{
		return instance;
	}
	
	@Override
	public void onEnableInner()
	{
		debug("Loading plugin configuration from file...");
		loadPluginConfiguration();
		debug("Plugin configuration loaded successfully.");
		debug("Registering listeners for...");
		registerListeners();
		debug("All listeners registered successfully.");
		debug("Registering custom plugin commands...");
		registerCommands();
		debug("All commands registered successfully.");
		debug("Enabling BossBarHandler...");
		new BossBarHandler();
		debug("BossBarHandler enabled successfully.");
	}
	
	@Override
	public @NotNull YamlConfiguration getConfig()
	{
		return pluginConfiguration;
	}
	
	private void loadPluginConfiguration()
	{
		if (getDataFolder().mkdirs()) debug("No plugin folder found. Creating new one...");
		configurationFile = new File(getDataFolder(), "config.yml");
		if (!configurationFile.exists())
		{
			debug("No custom configuration file found. Copying defaults...");
			InputStream resourcesConfig = getResource("config.yml");
			try
			{
				if (resourcesConfig == null)
				{
					log(Level.WARNING, "No default configuration file found. Perhaps your JAR-file is corrupted?");
					suicide();
					return;
				}
				Files.copy(resourcesConfig, configurationFile.toPath());
				log("Defaults copied successfully.");
			}
			catch (IOException e)
			{
				error(e);
			}
		}
		pluginConfiguration = YamlConfiguration.loadConfiguration(configurationFile);
		
		if (!pluginConfiguration.contains("allgemein.titel"))
		{
			pluginConfiguration.set("allgemein.titel", "DreamStreamer Community Server");
		}
		if (!pluginConfiguration.contains("allgemein.farbe"))
		{
			pluginConfiguration.set("allgemein.farbe", "purple");
		}
		if (!pluginConfiguration.contains("allgemein.stil"))
		{
			pluginConfiguration.set("allgemein.stil", "progress");
		}
		
		if (!savePluginConfiguration()) suicide();
		reloadPluginConfiguration();
	}
	
	public void reloadPluginConfiguration()
	{
		pluginConfiguration = YamlConfiguration.loadConfiguration(configurationFile);
	}
	
	public boolean savePluginConfiguration()
	{
		try
		{
			pluginConfiguration.save(configurationFile);
		}
		catch (IOException e)
		{
			error(e);
			return false;
		}
		return true;
	}
	
	private void registerListeners()
	{
		new JoinListener();
	}
	
	private void registerCommands()
	{
		CommandAPI.registerCommand(BaseCommand.class);
	}
	
	public Component translateColorCodes(String message)
	{
		LegacyComponentSerializer serializer = LegacyComponentSerializer.builder().character('&').build();
		return serializer.deserialize(message);
	}
	
	public String getPrefix()
	{
		return getDescription().getPrefix();
	}
}
