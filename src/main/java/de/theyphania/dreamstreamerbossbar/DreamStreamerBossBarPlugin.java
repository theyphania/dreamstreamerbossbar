package de.theyphania.dreamstreamerbossbar;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.awt.Color;
import java.util.logging.Level;

public abstract class DreamStreamerBossBarPlugin extends JavaPlugin
{
	private String logPrefixColored;
	private String logPrefixPlain;
	private ChatColor dark = ChatColor.of(new Color(0x0D0D0D));
	private ChatColor light = ChatColor.of(new Color(0x888888));
	private ChatColor primary = ChatColor.of(new Color(0x2B3966));
	private long enableTime;
	
	@Override
	public void onLoad()
	{
		onLoadPre();
		onLoadInner();
		onLoadPost();
	}
	
	private void onLoadPre()
	{
		logPrefixColored = dark + "[" + primary + getDescription().getPrefix() + dark + "]" + light;
		logPrefixPlain = ChatColor.stripColor(logPrefixColored);
	}
	
	private void onLoadInner()
	{
	}
	
	private void onLoadPost()
	{
	}
	
	public long getEnableTime()
	{
		return enableTime;
	}
	
	@Override
	public void onEnable()
	{
		onEnablePre();
		onEnableInner();
		onEnablePost();
	}
	
	private void onEnablePre()
	{
		enableTime = System.currentTimeMillis();
		log("// ========== STARTING PLUGIN ========== //");
	}
	
	public void onEnableInner()
	{
	}
	
	private void onEnablePost()
	{
		long ms = System.currentTimeMillis() - enableTime;
		log("// ========== DONE (Time: " + ms + "ms) ========== //");
	}
	
	@Override
	public void onDisable()
	{
	}
	
	public void suicide()
	{
		log(Level.WARNING, "Plugin will be killed!");
		Bukkit.getPluginManager().disablePlugin(this);
	}
	
	public void debug(String debugString)
	{
		log("DEBUG", debugString);
	}
	
	public void error(Exception e)
	{
		log(Level.SEVERE, "Looks like we have a " + e.toString() + " here.");
		log(Level.SEVERE, "Here is a bit more detail:");
		e.printStackTrace();
	}
	
	public void log(String prefixString, String messageString)
	{
		log(Level.INFO, "[" + prefixString + "] " + messageString);
	}
	
	public void log(String messageString)
	{
		log(Level.INFO, messageString);
	}
	
	public void log(Level level, String messageString)
	{
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		if (level == Level.INFO)
		{
			console.sendMessage(logPrefixColored + " " + messageString);
		}
		else
		{
			Bukkit.getLogger().log(level, logPrefixPlain + " " + messageString);
		}
	}
}
